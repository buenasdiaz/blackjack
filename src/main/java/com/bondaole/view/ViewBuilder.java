package com.bondaole.view;

import com.bondaole.model.Card;

import java.util.List;

/**
 * Builds view for console.
 *
 * @author Oleksandr Bondarenko
 */
public class ViewBuilder {
    private static final String ASKING_PLAYER_TO_BET = "Please, enter the value of your bet.";
    private static final String GAME_OVER_MESSAGE = "Game is over.";
    private static final String PLAYER_HAS_BLACKJACK_MESSAGE = "You have blackjack. Win.";
    private static final String PLAYER_DEALER_HAVE_BLACKJACKS_MESSAGE = "You and dealer have blackjacks. Push.";
    private static final String DEALER_IS_BUSTED_MESSAGE = "Dealer is busted. Win.";
    private static final String PUSH_MESSAGE = "Dealer has the same value as you. Push.";
    private static final String PLAYER_HAS_WON_BY_POINTS_MESSAGE = "You have more than dealer. Win.";
    private static final String DEALER_HAS_WON_BY_POINTS_MESSAGE = "You have less than dealer. Lose.";
    private static final String PLAYER_IS_BUSTED_MESSAGE = "You are busted. Lose.";
    private static final String START_NEW_GAME_MESSAGE = " Would you like to start a new game? (Type y if yes)";
    private static final String HINT_TO_HIT_OR_STAND = "Please, type h for Hit or s for Stand.";
    private static final String PLAYER_S_CARDS_MARKER = "Player's: ";
    private static final String DEALER_S_CARDS_MARKER = "Dealer's: ";

    private boolean startNewGame;
    private boolean dealerBusted;
    private boolean playerBusted;
    private boolean bothHaveBlackJack;
    private boolean onlyPlayerHasBlackJack;
    private boolean dealerHasMore;
    private boolean helpToHitOrStand;
    private boolean bothHaveEqual;
    private boolean playerHasMore;
    private boolean doubleNotAllowed;
    private boolean requestBet;
    private boolean gameIsOver;
    private List<Card> playerHand;
    private List<Card> dealerHand;
    private Card dealerCard;

    public String buildView(){
        StringBuilder sb = new StringBuilder();
        if (requestBet)
            return sb.append(ASKING_PLAYER_TO_BET).toString();
        else if (gameIsOver)
            return sb.append(GAME_OVER_MESSAGE).toString();
        else if (onlyPlayerHasBlackJack)
            sb = sb.append(PLAYER_HAS_BLACKJACK_MESSAGE);
        else if (bothHaveBlackJack)
            sb.append(PLAYER_DEALER_HAVE_BLACKJACKS_MESSAGE);
        else if (dealerBusted)
            sb.append(DEALER_IS_BUSTED_MESSAGE);
        else if (bothHaveEqual)
            sb.append(PUSH_MESSAGE);
        else if (playerHasMore)
            sb.append(PLAYER_HAS_WON_BY_POINTS_MESSAGE);
        else if (dealerHasMore)
            sb.append(DEALER_HAS_WON_BY_POINTS_MESSAGE);
        else if (playerBusted)
            sb.append(PLAYER_IS_BUSTED_MESSAGE);
        if (startNewGame){
            StringBuilder sb0 = addCardsValues();
            sb0.append(sb);
            return sb0.append(START_NEW_GAME_MESSAGE).toString();
        }
        if (helpToHitOrStand) {
            sb.append(addCardsValues());
            return sb.append(HINT_TO_HIT_OR_STAND).toString();
        }
        return null;
    }

    private StringBuilder addCardsValues() {
        StringBuilder sb = new StringBuilder();
        sb.append(PLAYER_S_CARDS_MARKER);
        for (int i=0; i< playerHand.size();i++)
            if (i<playerHand.size()-1)
                sb.append(playerHand.get(i) + ", ");
            else
                sb.append(playerHand.get(i) + ". ");
        sb.append(DEALER_S_CARDS_MARKER);
        if (dealerHand != null)
            for (int i=0; i< dealerHand.size();i++)
                if (i<dealerHand.size()-1)
                    sb.append(dealerHand.get(i) + ", ");
                else
                    sb.append(dealerHand.get(i) + ". ");
        else
            sb.append(dealerCard + ". ");
        return sb;
    }

    public void addPlayerHand(List<Card> playerHand) {
        this.playerHand = playerHand;
    }

    public void setDealerHand(List<Card> dealerHand) {
        this.dealerHand = dealerHand;
    }

    public void setDealerCard(Card dealerCard) {
        this.dealerCard = dealerCard;
    }

    public void setStartNewGame(boolean startNewGame) {
        this.startNewGame = startNewGame;
    }

    public void setPlayerBusted(boolean playerBusted) {
        this.playerBusted = playerBusted;
    }

    public void setHelpToHitOrStand(boolean helpToHitOrStand) {
        this.helpToHitOrStand = helpToHitOrStand;
    }

    public void setDealerBusted(boolean dealerBusted) {
        this.dealerBusted = dealerBusted;
    }

    public void setBothHaveEqual(boolean bothHaveEqual) {
        this.bothHaveEqual = bothHaveEqual;
    }

    public void setPlayerHasMore(boolean playerHasMore) {
        this.playerHasMore = playerHasMore;
    }

    public void setDealerHasMore(boolean dealerHasMore) {
        this.dealerHasMore = dealerHasMore;
    }

    public void setDoubleNotAllowed(boolean doubleNotAllowed) {
        this.doubleNotAllowed = doubleNotAllowed;
    }

    public void setOnlyPlayerHasBlackJack(boolean onlyPlayerHasBlackJack) {
        this.onlyPlayerHasBlackJack = onlyPlayerHasBlackJack;
    }

    public void setBothHaveBlackJack(boolean bothHaveBlackJack) {
        this.bothHaveBlackJack = bothHaveBlackJack;
    }

    public void setRequestBet(boolean requestBet) {
        this.requestBet = requestBet;
    }

    public void setGameIsOver(boolean gameIsOver) {
        this.gameIsOver = gameIsOver;
    }
}
