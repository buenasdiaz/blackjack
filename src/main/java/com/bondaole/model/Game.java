package com.bondaole.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Tracks a game.
 *
 * @author Oleksandr Bondarenko
 */
public class Game {
    private final List<Card> dealerHand = new ArrayList<Card>();
    private final List<Card> playerHand = new ArrayList<Card>();
    private int playerValue;
    private int dealerValue;
    private int playerAces;
    private boolean softHand;
    private int bet;
    private boolean over;
    private Deck deck = new Deck();

    public Game(String bet) {
        this.bet = Integer.parseInt(bet);
    }

    public void addToPlayer(Card... cards) {
        for (Card card : cards) {
            addToPlayer(card);
        }
    }

    protected void addToPlayer(Card card) {
        playerHand.add(card);
        if (card.value == 11) {
            playerValue += 11;
            softHand = true;
            playerAces++;
            return;
        }
        playerValue += card.value;
    }

    public void addToDealer(Card... cards) {
        for (Card card : cards) {
            addToDealer(card);
        }
    }

    protected void addToDealer(Card card) {
        dealerHand.add(card);
        dealerValue += card.value;
    }


    public boolean playerHasBJ() {
        return playerValue == 21 ? true : false;
    }

    public boolean dealerHasBJ() {
        return dealerValue == 21 ? true : false;
    }

    public boolean isPlayerBusted() {
        if (playerValue > 21 && !softHand){
            over = true;
            return true;
        }
        int aces = playerAces;
        int value = playerValue;
        while (aces > 0) {
            aces--;
            value-=10;
            if (value>21){
                over = true;
                return true;
            }
        }
        return false;
    }

    public int getPlayerValue() {
        if (!softHand || playerValue<22)
            return playerValue;
        int aces = playerAces;
        int value = playerValue;
        while (aces > 0) {
            aces--;
            value-=10;
            if (value<=21)
                return value;
        }
        return value;
    }

    public void addNextCardToPlayer(){
        addToPlayer(deck.cards.remove(0));
    }

    public void addNextCardToDealer(){
        addToDealer(deck.cards.remove(0));
    }

    public void dealInitialCards(){
        addToPlayer(deck.cards.remove(0), deck.cards.remove(2));
        addToDealer(deck.cards.remove(1), deck.cards.remove(3));
    }

    public boolean isOver() {
        return over;
    }

    public void setOver(boolean over) {
        this.over = over;
    }

    public boolean isDealerBusted(){
        return dealerValue > 21;
    }

    public boolean bothHaveEqual(){
        return dealerValue == getPlayerValue();
    }

    public boolean playerHasMore(){
        return dealerValue < getPlayerValue();
    }

    public boolean dealerHasMore(){
        return dealerValue > getPlayerValue();
    }

    public boolean dealerHasToHit(){
        return dealerValue < 17;
    }

    public boolean isDoubleNotAllowed(){
        return playerHand.size() != 2;
    }

    public List<Card> getPlayerHand() {
        return playerHand;
    }

    public List<Card> getDealerHand() {
        return dealerHand;
    }

    public Card getFirstDealerCard(){
        return dealerHand.get(0);
    }
}