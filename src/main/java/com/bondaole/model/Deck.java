package com.bondaole.model;

import java.util.*;

/**
 * Class representing a deck of cards.
 *
 * @author Oleksandr Bondarenko
 */
class Deck {
    final List<Card> cards = new ArrayList<Card>();
    static final Map<String, Integer> nameToValue = new HashMap<String, Integer>();

    static {
        nameToValue.put("2", 2);
        nameToValue.put("3", 3);
        nameToValue.put("4", 4);
        nameToValue.put("5", 5);
        nameToValue.put("6", 6);
        nameToValue.put("7", 7);
        nameToValue.put("8", 8);
        nameToValue.put("9", 9);
        nameToValue.put("10", 10);
        nameToValue.put("Jack", 10);
        nameToValue.put("Queen", 10);
        nameToValue.put("King", 10);
        nameToValue.put("Ace", 11);
    }

    Deck() {
        for (Map.Entry<String, Integer> entry : nameToValue.entrySet()) {
            for (int i = 0; i < 4; i++) {
                cards.add(new Card(entry.getValue(), entry.getKey()));
            }
        }
        Collections.shuffle(cards);
    }
}
