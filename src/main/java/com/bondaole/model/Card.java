package com.bondaole.model;

/**
 * Template for cards creation.
 *
 * @author Oleksandr Bondarenko
 */
public class Card {
    final int value;
    final String name;

    Card(int value, String name) {
        this.value = value;
        this.name = name;
    }

    @Override
    public String toString(){
        return name;
    }
}
