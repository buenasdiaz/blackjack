package com.bondaole;

import com.bondaole.controller.Router;
import com.bondaole.view.ViewBuilder;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * Server that accepts data from clients, processes them and sends the response.
 *
 * @author Oleksandr Bondarenko
 */
public class Dealer {
    static Router router = new Router();

    public static void main(String[] args) throws IOException {
        int port = 8080;
        if (args.length > 0)
            port = Integer.parseInt(args[0]);

        ServerSocket myServer = null;
        try {
            myServer = new ServerSocket(port);
        } catch (IOException e) {
            System.err.println("Can't listen on this port.");
        }

        Socket socket = null;
        try {
            socket = myServer.accept();
        } catch (IOException e) {
            System.err.println("Accept failed.");
        }

        BufferedReader reader = getBufferedReader(socket);
        PrintWriter writer = getPrintWriter(socket);

        String message;
        ViewBuilder backMessage;

        while ((message = reader.readLine()) != null) {
            backMessage = router.route(message);
            writer.println(backMessage.buildView());
        }

        reader.close();
        writer.close();
        socket.close();
        myServer.close();
    }

    protected static BufferedReader getBufferedReader(Socket socket) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        return reader;
    }

    protected static PrintWriter getPrintWriter(Socket socket) throws IOException {
        PrintWriter writer = new PrintWriter(socket.getOutputStream(), true);
        return writer;
    }
}
