package com.bondaole;

/**
 * States of the game
 *
 * @author Oleksandr Bondarenko
 */
public class Routes {
    public static final int HIT = 0;
    public static final int BET = 1;
    public static final int FIRST = 2;
    public static final int NEW = 3;
}
