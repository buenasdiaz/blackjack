package com.bondaole.controller;

import com.bondaole.model.Game;
import com.bondaole.view.ViewBuilder;

import java.util.HashMap;
import java.util.Map;

import static com.bondaole.Routes.*;

/**
 * Dispatches requests across handler methods.
 *
 * @author Oleksandr Bondarenko
 */
public class Router {
    private static final String PLAYER_AGREED_TO_START_NEW_GAME = "y";
    private static final String PLAYER_HITS = "h";
    private static final String PLAYER_STANDS = "s";
    private static final String PLAYER_DOUBLES = "d";

    private Map<Integer, Integer> paths = new HashMap<Integer, Integer>();

    private Game game;

    public Router() {
        initPaths();
    }

    Integer previousStep;

    private void initPaths() {
        paths.put(null, FIRST);
        paths.put(FIRST, BET);
        paths.put(BET, HIT);
        paths.put(NEW, FIRST);
    }

    public ViewBuilder route(String message) {
        ViewBuilder viewBuilder = new ViewBuilder();
        Integer path = paths.get(previousStep);
        switch (path) {
            case FIRST:
                previousStep = FIRST;
                requestBet(message, viewBuilder);
                break;
            case BET:
                previousStep = BET;
                initCards(message, viewBuilder);
                addCards(viewBuilder);
                break;
            case HIT:
                if (PLAYER_HITS.equalsIgnoreCase(message)) {
                    hit(viewBuilder);
                    addCards(viewBuilder);
                } else if (PLAYER_STANDS.equalsIgnoreCase(message)) {
                    viewBuilder = stand(viewBuilder);
                    addCards(viewBuilder);
                } else if (PLAYER_DOUBLES.equalsIgnoreCase(message)) {
                    viewBuilder = doubleDown(viewBuilder);
                    addCards(viewBuilder);
                }
                break;
            default:
                System.err.println("Path " + path + " is not supported.");
        }
        if (game != null && game.isOver())
            return startNewGameOrNot(viewBuilder);
        return viewBuilder;
    }

    private ViewBuilder startNewGameOrNot(ViewBuilder viewBuilder) {
        previousStep = NEW;
        game = null;
        viewBuilder.setStartNewGame(true);
        return viewBuilder;
    }

    private ViewBuilder addCards(ViewBuilder viewBuilder) {
        viewBuilder.addPlayerHand(game.getPlayerHand());
        if (game.isOver())
            viewBuilder.setDealerHand(game.getDealerHand());
        else
            viewBuilder.setDealerCard(game.getFirstDealerCard());
        return viewBuilder;
    }

    private ViewBuilder hit(ViewBuilder viewBuilder) {
        game.addNextCardToPlayer();
        if (game.isPlayerBusted()) {
            viewBuilder.setPlayerBusted(true);
            return viewBuilder;
        }
        viewBuilder.setHelpToHitOrStand(true);
        return viewBuilder;
    }

    private ViewBuilder stand(ViewBuilder viewBuilder) {
        while (game.dealerHasToHit()) {
            game.addNextCardToDealer();
        }
        if (game.isDealerBusted()) {
            game.setOver(true);
            viewBuilder.setDealerBusted(true);
        }
        else if (game.bothHaveEqual()) {
            game.setOver(true);
            viewBuilder.setBothHaveEqual(true);
        }
        else if (game.playerHasMore()) {
            game.setOver(true);
            viewBuilder.setPlayerHasMore(true);
        }
        else if (game.dealerHasMore()) {
            game.setOver(true);
            viewBuilder.setDealerHasMore(true);
        }
        return viewBuilder;
    }

    private ViewBuilder doubleDown(ViewBuilder viewBuilder) {
        if (game.isDoubleNotAllowed()) {
            viewBuilder.setDoubleNotAllowed(true);
            return viewBuilder;
        }
        game.addNextCardToPlayer();
        return stand(viewBuilder);
    }

    private ViewBuilder initCards(String bet, ViewBuilder viewBuilder) {
        createGame(bet);
        if (game.playerHasBJ() && !game.dealerHasBJ()){
            game.setOver(true);
            viewBuilder.setOnlyPlayerHasBlackJack(true);
        } else if (game.playerHasBJ() && !game.dealerHasBJ()) {
            game.setOver(true);
            viewBuilder.setBothHaveBlackJack(true);
        }
        if (!game.isOver()){
            viewBuilder.setHelpToHitOrStand(true);
        }
        return viewBuilder;
    }

    private void createGame(String bet) {
        Game game = new Game(bet);
        game.dealInitialCards();
        this.game = game;
    }

    private ViewBuilder requestBet(String message, ViewBuilder viewBuilder) {
        if (PLAYER_AGREED_TO_START_NEW_GAME.equalsIgnoreCase(message)
                || String.valueOf(FIRST).equalsIgnoreCase(message))
            viewBuilder.setRequestBet(true);
        else
            viewBuilder.setGameIsOver(true);
        return viewBuilder;
    }
}