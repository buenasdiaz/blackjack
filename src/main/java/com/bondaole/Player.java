package com.bondaole;

import java.io.*;
import java.net.Socket;

import static com.bondaole.Dealer.getBufferedReader;
import static com.bondaole.Dealer.getPrintWriter;
import static java.lang.System.in;

/**
 * Client that reads player's input from the command-line and sends it to the server.
 *
 * @author Oleksandr Bondarenko
 */
public class Player {

    public static void main(String[] args) throws IOException {
        int port = 8080;
        if (args.length > 0)
            port = Integer.parseInt(args[0]);

        Socket socket = null;
        try {
            socket = new Socket("localhost", port);
        } catch (IOException e) {
            System.err.println("Err – " + e);
        }

        BufferedReader reader = getBufferedReader(socket);
        PrintWriter writer = getPrintWriter(socket);
        BufferedReader stdIn = new BufferedReader(new InputStreamReader(in));

        writer.println(String.valueOf(Routes.FIRST));

        String fromServer;
        String fromUser;

        while ((fromServer = reader.readLine()) != null) {
            System.out.println("Dealer: " + fromServer);
            if (fromServer.equals("Game is over."))
                break;

            fromUser = stdIn.readLine();
            if (fromUser != null) {
                writer.println(fromUser);
            }
        }

    }
}